package io.jsko.jon.Shelly.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.jsko.jon.Shelly.Main;
import io.jsko.jon.Shelly.Shell.Shell;

public class Commands {
	public Main plugin;

	String[] args;

	public Commands(Main instance) {
		plugin = instance;
	}	
	
	@SuppressWarnings("unused")
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		String commandName = command.getName().toLowerCase();
		if (!(sender instanceof Player)) {
			Main.log.info("This command must be run in game.");
			return true;
		} else {
			final Player player = (Player) sender;
			if (commandName.equalsIgnoreCase("shell")) {
				if (player.isOp()) {
					if (args.length == 0) {
						player.sendMessage("More arguements.");
					} else {
						StringBuilder builder = new StringBuilder();
						for (int i = 0; i < args.length; i++) {
						    builder.append(args[i]).append(" ");
						}
						String msg = builder.toString();
						
						Shell.cmd(msg.toString(), player);
					}
				} else {
					player.sendMessage(Main.hiddenCmd);
				}
			}
		}

		// End of Command
		return true;
	}	
}
