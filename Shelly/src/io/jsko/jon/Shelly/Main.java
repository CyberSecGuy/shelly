package io.jsko.jon.Shelly;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import io.jsko.jon.Shelly.Commands.Commands;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin implements Listener {
	// === Cookie Cutter ===
	Server _server = null;
	public static JavaPlugin _parent = null;
	public String _datafolder;
	public boolean _isShutdown = false;
	public static Logger log = Logger.getLogger("Minecraft");

	public Main plugin = this;
	
	// === Command Classes === //
	public Commands cmd = new Commands(plugin);
	
	// === Config Class === //
	//public Config conf = new Config(plugin);
	
	// === Config === //
//	public File pluginFolder = getDataFolder();
//	public File cfgFile = new File(pluginFolder, "config.yml");
//	public FileConfiguration cfg = new YamlConfiguration();
//	
//	public static FileConfiguration langData = new YamlConfiguration();
//	
//	public static FileConfiguration playerData = new YamlConfiguration();
		
	// == Config Strings == //
	public static Boolean debugMode = false;
	public static int updateInterval = 10;
	public static List<String> disableWorlds;
	
	// === Language Strings === //
	public static String hiddenCmd = "Unknown command. Type \"/help\" for help.";	
	public static String reloadCfg = ChatColor.AQUA + "Config file reloaded!";
	
	// === Plugin Version === //
	public static String version;

	public void getVersion() {
		PluginDescriptionFile pdfFile = getDescription();
		version = pdfFile.getVersion();
	}
	
	public static int taskID;
	
	// Enable
	@Override
	public void onEnable() {
		log = this.getLogger();
		Initialize(getServer(), this, getDataFolder().getAbsolutePath() + "/");
//		saveDefaultConfig();
	}

	// Initialize
	public boolean Initialize(Server server, JavaPlugin parent, String dataFolder) {
		this._server = server;
		Main._parent = parent;
		this._datafolder = dataFolder;
		getServer().getPluginManager().registerEvents(this, this);
		return true;
	}
		
	@Override
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		cmd.onCommand(sender, command, commandLabel, args);
		return true;
	}
	
	// === Disable ===
	public void onDisable() {
//		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
//			DataControl.writeData(this, player);
//		}
	}
}
